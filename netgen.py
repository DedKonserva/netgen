﻿from tkinter import *
import math

modal = 1
win = Tk()
#win.overrideredirect(1)
win.withdraw()
win2 = Tk()
#win2.overrideredirect(1)
win2.withdraw()

class MyApp (object):
	def __init__(self, parent):
		self.root = parent
		self.root.title("Генератор ЛВС")
		self.frame = Tk.Frame(parent)
		self.frame.pack()
	
def dialog(arr, rnum, snum, router, switch):
	#print (arr)
	#print ("\n")
	#print("trying to show window")
	win.deiconify()
	modal = 1
	#print("deiconified")
	hgt = snum*60 + 30
	#frame.delete("all")
	
	frame = Canvas(win, width=130, height=hgt)
	frame.pack()
	
	
	allIP1 = []
	allIP2 = []
	allIP3 = []
	allIP4 = []
	
	for n in range(snum):
		lbl1 = Label(frame, text="IP сети "+str(n) +":")
		lbl1.place(x=65, y=60*n+10, anchor="center")

		ip1En = Entry(frame, width = 3)
		ip1En.insert(0, 0)
		allIP1.append(ip1En)
		allIP1[n].place(x=10, y=60*n+30)
		
		ip2En = Entry(frame, width = 3)
		ip2En.insert(0, 0)
		allIP2.append(ip2En)
		allIP2[n].place(x=40, y=60*n+30)
		
		ip3En = Entry(frame, width = 3)
		ip3En.insert(0, 0)
		allIP3.append(ip3En)
		allIP3[n].place(x=70, y=60*n+30)
		
		ip4En = Entry(frame, width = 3)
		ip4En.insert(0, 0)
		allIP4.append(ip4En)
		allIP4[n].place(x=100, y=60*n+30)
		
	okBtn = Button(frame, text="OK")	
	okBtn.place(x=10, y=60*snum)
	okBtn.bind("<Button-1>", lambda arg: setPortIP(win, arr, rnum, snum, allIP1, allIP2, allIP3, allIP4, router, switch))

	if (modal == 1):
		win.focus_set()
		win.grab_set()
		win.wait_window()
	else:
		win.destroy()
	
	
def setPortIP(form, arr, rnum, snum, ip1, ip2, ip3, ip4, router, switch):
	subnetOffset = 0
	for r in range(rnum):
		for s in range(router[r]):
			#print (str(r) + ":" + str(s))
			arr[r*32+s*4+0]=ip1[subnetOffset+s].get()
			arr[r*32+s*4+1]=ip2[subnetOffset+s].get()
			arr[r*32+s*4+2]=ip3[subnetOffset+s].get()
			arr[r*32+s*4+3]=ip4[subnetOffset+s].get()
		subnetOffset = subnetOffset + router[r]	
	#print(arr)
	modal = 0
	form.destroy()


def upd():
	#print ("update\n")	
	canvas.delete("all")
	rectsize = 30
	offset = 10
	globalHeightOffset = 80
	#text1.delete('1.0', END)
	tHeight = int(treeHeightEn.get()) -1
	if (tHeight > 0):
		sNum = int(subnetNumEn.get())
		leafNum = int(leafNumEn.get())
		router = [0] * (tHeight)
		switch = [0] * (sNum)
		
		IPArr = [0]*4*8*tHeight
		
		tmpSNum = sNum
		while(tmpSNum>0):
			for i in range(tHeight):
				if (tmpSNum>0):
					router[i]= router[i]+1
					tmpSNum = tmpSNum - 1
		
		tmpLeafNum = leafNum
		while(tmpLeafNum>0):
			#print(str(tmpLeafNum))
			for s in range(sNum):
				#print("switch " + str(s) +"\n")
				if (tmpLeafNum>0):
					#print("added 1 to switch\n")
					switch[s] = switch[s] + 1
					tmpLeafNum = tmpLeafNum - 1
					
		#for s in range(sNum):
			#print (str(s) + ": " + str(switch[s]))
			
		switchOffset = 0
		maxLevelLeafs = 0
		for i in range(tHeight):
			#print("scanning router " +str(i) + "\n")
			levelLeafs = 0
			for k in range (router[i]):
				levelLeafs = levelLeafs + switch[k+switchOffset]
			if (levelLeafs > maxLevelLeafs):
				maxLevelLeafs = levelLeafs
			switchOffset = switchOffset + router[i]
			
		#text1.insert(1.0, str(maxLevelLeafs) + "\n")
		
		switchOffset = 0
		for i in range(tHeight):
			switchDrawOffset = 0
			leafDrawOffset = 0
			#text1.insert(1.0, str(router[i])+"\n")
			routerHeightOffset = globalHeightOffset+(offset + 2*rectsize + 2*offset)* i +10
			rectangle = canvas.create_rectangle(360, (rectsize)*i + routerHeightOffset, 360+rectsize, (rectsize)*(i+1) + routerHeightOffset, fill = "grey")
			for k in range(router[i]):
				rectangle = canvas.create_rectangle(switchDrawOffset + 60, rectsize*(i+1) + routerHeightOffset +10, switchDrawOffset+rectsize+60, (rectsize)*(i+2) + routerHeightOffset + 10, fill = "grey")
				switchDrawOffset = switchDrawOffset + switch[k+switchOffset]*(rectsize +10)
				for l in range(switch[k+switchOffset]):
					circle = canvas.create_oval(leafDrawOffset + (rectsize+10)*l + 60, rectsize*(i+2) + routerHeightOffset + 20, leafDrawOffset+rectsize+(rectsize+10)*l + 60, rectsize*(i+3) + routerHeightOffset + 20, fill = "grey")
				leafDrawOffset = leafDrawOffset + switch[k+switchOffset]*(rectsize+10)
			switchOffset = switchOffset + router[i]
		
		dialog(IPArr, tHeight, sNum, router, switch)
		
		switchOffset = 0
		PCNum = 0
		for r in range(tHeight):
			for s in range(router[r]):
				for p in range(switch[s+switchOffset]):
					print ("PC " + str(PCNum) + " interface: ")
					newip = int(IPArr[r*32+s*4+3])+p+1
					print ("eth0 " + str(IPArr[r*32+s*4+0]) + "." + str(IPArr[r*32+s*4+1]) + "." + str(IPArr[r*32+s*4+2]) + "." + str((newip)))
					print ("Routing table: ")
					print ("Target | Gateway ")
					print (str(IPArr[r*32+s*4+0]) + "." + str(IPArr[r*32+s*4+1]) + "." + str(IPArr[r*32+s*4+2]) + "." + str(IPArr[r*32+s*4+3]) + "| *" )
					newip2 = int(IPArr[r*32+s*4+3])+int(switch[s+switchOffset])+1
					print ("0.0.0.0 | " + str(IPArr[r*32+s*4+0]) + "." + str(IPArr[r*32+s*4+1]) + "." + str(IPArr[r*32+s*4+2]) + "." + str(newip2)+"\n")
					PCNum = PCNum + 1
					

		
		for r in range(tHeight):
			#print ("NET RANGE: FROM "+str(netNumBeg) + " TO " + str(netNumEnd))
			print ("Router " + str(r))
			print ("Interfaces:\nInterface Status IP")
			for i in range(router[r]):
				print ("eth"+str(i)+" UP " + str(IPArr[r*32+i*4+0]) + "." + str(IPArr[r*32+i*4+1]) + "." + str(IPArr[r*32+i*4+2]) + "." + str(IPArr[r*32+i*4+3]))
			for i in range(6-router[r]):
				print ("eth"+str(i+router[r]) + " DOWN")
			if (r>0):
				print ("eth6 UP 10.0.0." + str(r*2))
			elif ((r==0) and (router[r]<7)):
				print("eth6 DOWN")
			if (router[r]<8):
				if (r+1<tHeight):
					print ("eth7 UP 10.0.0." + str(r*2+1))
				else:
					print ("eth7 DOWN")
			
			print ("\nRouting table:\nTarget Gateway Interface")
			for r2 in range(tHeight):
				for s in range(router[r2]):
					#print ("S: "+str(s))
					if (r2==r):
						print (str(IPArr[r2*32+s*4+0]) + "." + str(IPArr[r2*32+s*4+1]) + "." + str(IPArr[r2*32+s*4+2]) + "." + str(IPArr[r2*32+s*4+3]) + " * " + "eth"+str(s))
					elif (r2<r):
						print (str(IPArr[r2*32+s*4+0]) + "." + str(IPArr[r2*32+s*4+1]) + "." + str(IPArr[r2*32+s*4+2]) + "." + str(IPArr[r2*32+s*4+3]) + " * eth6")
					else:
						print (str(IPArr[r2*32+s*4+0]) + "." + str(IPArr[r2*32+s*4+1]) + "." + str(IPArr[r2*32+s*4+2]) + "." + str(IPArr[r2*32+s*4+3]) + " * eth7")
			print("\n")	
	else:
		print ("Недопустимая высота дерева")
		
	
root = Tk()
root.title("Генератор ЛВС")

canvas = Canvas(root, width=800, height = 600)
canvas.pack()


updBtn = Button(canvas, text="Обновить", command = upd)
updBtn.place(x = 220, y = 20, anchor = NW)

lblHeight = Label(canvas, text="Высота дерева: ")
lblHeight.place(x = 10, y = 20)
treeHeightEn = Entry(canvas, width = 4)
treeHeightEn.place(x = 180, y = 20, anchor = NW)

lblSubnet = Label(canvas, text="Количество подсетей: ")
lblSubnet.place(x = 10, y = 40)
subnetNumEn = Entry(canvas, width = 4)
subnetNumEn.place(x = 180, y = 40, anchor = NW)

lblLeafs = Label(canvas, text="Количество компьютеров: ")
lblLeafs.place(x = 10, y = 60)
leafNumEn = Entry(canvas, width = 4)
leafNumEn.place(x = 180, y = 60, anchor = NW)

#text1 = Text(canvas, height = 7, width = 7)
#text1.place(x = 10, y = 80, anchor = NW)
root.mainloop()

